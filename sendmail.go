package sendmail

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/streadway/amqp"
	"os"
)

type Template string

const (
	RequestResetPassword Template = "request_reset_password"
	NewAccount Template = "new_account"
)

type ConfirmationFunc func(confirms <-chan amqp.Confirmation)

type Address struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

type Params map[string]interface{}

type Email struct {
	template Template
	from     *Address
	to       *Address
	params   *Params
}

func NewEmail(t Template) *Email {
	return &Email{template: t}
}

func (e *Email) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Template Template `json:"template"`
		From     *Address `json:"from,omitempty"`
		To       *Address `json:"to"`
		Params   *Params  `json:"params"`
	}{
		Template: e.template,
		From:     e.from,
		To:       e.to,
		Params:   e.params,
	})
}

func (e *Email) From(from *Address) *Email {
	e.from = from
	return e
}

func (e *Email) To(to *Address) *Email {
	e.to = to
	return e
}

func (e *Email) Params(p *Params) *Email {
	e.params = p
	return e
}

func (e *Email) Send(uri, ex string) error {
	return e.SendWithConfirmation(uri, ex, nil)
}

func (e *Email) SendWithConfirmation(uri, ex string, confirmFunc ConfirmationFunc) error {
	body, err := json.Marshal(e)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Failed to encode message %v", e))
	}
	err = publish(uri, ex, "direct", "", body, confirmFunc)
	if err != nil {
		return errors.Wrap(err, "Failed to publish a message")
	}
	return nil
}

func newUUID() uuid.UUID {
	for cpt := 0; cpt < 5; cpt++ {
		idV4, err := uuid.NewV4()
		if err == nil {
			return idV4
		}
	}
	panic("Cannot create UUID")
}

func publish(amqpURI, exchange, exchangeType, routingKey string, body []byte, confirmFunc ConfirmationFunc) error {
	connection, err := amqp.Dial(amqpURI)
	if err != nil {
		return errors.Wrapf(err, "Dial fail: %s", amqpURI)
	}
	defer connection.Close()

	channel, err := connection.Channel()
	if err != nil {
		return errors.Wrapf(err, "Fail to open channel")
	}

	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return errors.Wrapf(err, "Fail declare Exchange: %s %s", exchange, exchangeType)
	}

	if confirmFunc != nil {
		if err := channel.Confirm(false); err != nil {
			return errors.Wrapf(err, "Channel could not be put into confirm mode")
		}
		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))
		defer confirmFunc(confirms)
	}

	if err = channel.Publish(
		exchange,   // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			AppId:           os.Getenv("APP_ID"),
			MessageId:       newUUID().String(),
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            body,
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return errors.Wrapf(err, "Fail to publish")
	}

	return nil
}

//func confirmOne(confirms <-chan amqp.Confirmation) {
//	if confirmed := <-confirms; confirmed.Ack {
//		log.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
//	} else {
//		log.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
//	}
//}
